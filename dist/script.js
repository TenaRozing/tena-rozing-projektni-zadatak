
function toggleMenu() {
   var box = document.getElementById("toggle-class");
   var text = document.getElementById("toggle-green");
   var svg = document.getElementById("toggle-svg");
   box.classList.toggle("hidden-class");
   text.classList.toggle("green-class");
   svg.classList.toggle("svg-rotate");
}

function toggleMenuDesktop() {
   var box = document.getElementById("toggle-class-desktop");
   var text = document.getElementById("toggle-green-desktop");
   var svg = document.getElementById("toggle-svg-desktop");
   box.classList.toggle("hidden-class-desktop");
   text.classList.toggle("green-class-desktop");
   svg.classList.toggle("svg-rotate-desktop");
}

